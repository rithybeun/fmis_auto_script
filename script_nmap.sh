#!/bin/bash
#
# Check if an IP address is provided as an argument
function nmap_auto_scan(){
if [ -z "$1" ]; then
    echo "Usage: $0 <IP_address>"
    exit 1
fi

# Store the provided IP address
target_ip="$1"

# Path to store
location_path=$(pwd)

new_directory="/Nmap_Result"

if [ -d "$location_path$new_directory" ]; then
	echo "Directory Nmap_Result' already exists."
	exit 1
else
	# Create folder
	mkdir "$location_path$new_directory"
	echo "Directory Nmap_Result created successfully."
fi

# Run Nmap commands
echo "Running Nmap commands on $target_ip:"

# Command 1: Scan all 65535 ports
echo "1. Scanning all ports ..."
open_ports=$(nmap -p1-1000 -oN "$location_path$new_directory"/Nmap_Scan_All_Port "$target_ip" -T4 | grep ^[0-9] | cut -d '/' -f 1 | tr '\n' ',' | sed 's/.$//')
echo "Scan all port completed filename: $location_path$new_directory/Nmap_Scan_All_Port" 
echo "All port rithy $open_ports"

# Command 2: Scan all ports availble from scanning b4
echo "2. Scanning all ports availble from scanning b4 with script and version..."
sc_sv=$(nmap -sC -sV -p "$open_ports" -oN "$location_path$new_directory"/Nmap_Scan_Open_Port_SC_SV "$target_ip")
echo "Scanning Completed filename: $location_path$new_directory/Nmap_Scan_Open_Port_SC_SV"

# Command 3: Scan all ports availble from scanning b4 with script vuln 
echo "3. Scanning all port availble from scanning b4 with script vuln ..."
vuln_scan=$(nmap -p "$open_ports" --script vuln -oN "$location_path$new_directory"/Vuln_Nmap_Scan_Open_Port "$target_ip")
echo "Scaning Completeed filename: "$location_path$new_directory"/Vuln_Nmap_Scan_Open_Port"

# End of script
#
}
