#!/bin/bash

# Define your GitLab project URL
gitlab_project="https://gitlab.com/rithybeun/fmis_auto_script.git"

# Define the version file
version_file="version.txt"

# Read the local version from the file
local_version=$(cat "$version_file")

# Clone the repository to a temporary directory
#temp_dir=$(mktemp -d)
#git clone "$gitlab_project" "$temp_dir"

# Check the latest version on GitLab
#latest_version=$(git -C "$temp_dir" tag -l | sort -V | tail -n 1)

latest_version=$(git ls-remote --tags origin | awk -F '/' '{print $3}' | sort -V | tail -n 1)

# Compare versions
if [ "$latest_version" != "$local_version" ]; then
    echo "New version available. Updating..."

    # Pull the latest changes
    git pull origin main

    # Update the local version file
    echo "$latest_version" > "$version_file"

    # Copy other files or perform update tasks if needed

    echo "Update complete. Local version is now $latest_version."
else
    echo "Local version is up to date. No update needed."
fi

# Clean up the temporary directory
rm -rf "$temp_dir"

