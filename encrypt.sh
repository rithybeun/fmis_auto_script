#!/bin/bash

# Set the paths for input and output files
input_file="banner.sh"
encrypted_file="encrypted.enc"

# Generate a random 256-bit key (32 bytes)
encryption_key=$(openssl rand -hex 32)

# Encrypt the file using AES-256-CBC
openssl enc -aes-256-cbc -salt -in "$input_file" -out "$encrypted_file" -pass pass:"$encryption_key"

echo "File encrypted successfully. Encryption key: $encryption_key"

