#!/bin/bash

source script_nmap.sh

# Define ANSI color codes
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
BLUE='\033[0;34m'
RESET='\033[0m'

print_banner() {
ascii_art="""
_____ __  __ ___ ____  ____            _            _           _____           _     
|  ___|  \\/  |_ _/ ___||  _ \\ ___ _ __ | |_ ___  ___| |_ ___ _ _|_   _|__   ___ | |___ 
| |_  | |\\/| || |\\___ \\| |_) / _ \\ '_ \\| __/ _ \\/ __| __/ _ \\ '__|| |/ _ \\ / _ \\| / __|
|  _| | |  | || | ___) |  __/  __/ | | | ||  __/\\__ \\ ||  __/ |   | | (_) | (_) | \\__ \\
|_|   |_|  |_|___|____/|_|   \\___|_| |_|\\__\\___||___/\\__\\___|_|   |_|\\___/ \\___/|_|___/
"""

echo "$ascii_art"
}


# Function to display the menu
show_menu() {
    clear
    print_banner
    echo -e "${YELLOW}========== Menu ==========${RESET}"
    echo -e "1. ${RED}Nmap Auto Scan All${RESET}"
    echo -e "2. ${GREEN}Option 2${RESET}"
    echo -e "3. ${BLUE}Option 3${RESET}"
    echo -e "4. Go Back"
    echo -e "5. Exit"
    echo -e "${YELLOW}==========================${RESET}"

}

# Function to handle user input
handle_input() {
    read -p "Enter your choice: " choice
    case $choice in
        1)
            echo -e "${RED}Nmap Auto Scan All${RESET}"
	    read -p "Input IP: " ip
	    nmap_auto_scan $ip
            ;;
        2)
            echo -e "${GREEN}You selected Option 2${RESET}"
            ;;
        3)
            echo -e "${BLUE}You selected Option 3${RESET}"
            ;;
        4)
            return  # Go back (exit the function)
            ;;
        5)
            echo -e "Good Bye"
                exit 0
                ;;
        *)
            echo "Invalid choice. Please try again."
            ;;
    esac
}

# Main script
while true; do
	show_menu
    handle_input
    read -p "Press Enter to continue..."
done

