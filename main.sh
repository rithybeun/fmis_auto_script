#!/bin/bash

# Set the GitLab repository URL and branch
repository_url="https://gitlab.com/rithybeun/fmis_auto_script.git"
branch="main"

# Set the paths for the version and update check files
version_file="version.txt"
update_check_file="update_check.txt"

# Function to perform the update
perform_update() {
    echo "Performing update..."
    git pull origin "$branch"
    echo "Update complete."
    bash home.sh
}

# Check for updates
git fetch origin --tags
latest_version=$(git tag -l | sort -V | tail -n 1)

if [ "$latest_version" != "$(cat $version_file)" ]; then
    echo "New version available: $latest_version"
    
    # Check if update is allowed (avoid continuous updates)
    last_update_check=$(cat "$update_check_file")
    current_time=$(date +%s)
    update_interval=86400  # 24 hours

    if [ "$((current_time - last_update_check))" -ge "$update_interval" ]; then
        # Update the version file
        echo "$latest_version" > "$version_file"
        # Record the update check time
        echo "$current_time" > "$update_check_file"
        # Perform the update
        perform_update
    else
        echo "Update check skipped. Waiting for the next update interval."
    fi
else
    echo "No update needed. Script is up to date. $latest_version"
    bash home.sh
fi

